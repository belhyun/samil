import React, { Component } from 'react';
import DashBoard from './src/components/DashBoard';
import DevTools from 'mobx-react-devtools';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, Router} from "react-router-dom";
import BackgroundColorContainer from "./src/components/background/BackgroundColorContainer";
import { createBrowserHistory } from 'history'
import {setUpRootStore} from "./setUpRootStore";
import {Provider} from "mobx-react";
import SamilGraphqlClient from "./src/graphql/SamilGraphqlClient";
import {ApolloProvider} from "react-apollo"
import BackgroundPhotoContainer from "./src/components/background/photo/BackgroundPhotoContainer";

export const history = createBrowserHistory();
((r) => {
    r.keys().forEach(r)
// import './assets/css/first-step.css';
// @ts-ignore
})(require.context('./', true, /\.css$/));

class App extends Component {

    constructor(props) {
        super(props);
        // @ts-ignore
        history.listen((location, action) => {
        });
    }

    componentDidMount() {
        this.setState({
            rootStore: setUpRootStore()
        })
    }

    render() {

        // @ts-ignore
        const rootStore = this.state && this.state.rootStore

        if (!rootStore) {
            return null;
        }
        return (

            <ApolloProvider client={SamilGraphqlClient}>
                <Provider {...rootStore}>
                    <Router history={history}>
                        <div>
                            <Route exact path="/" component={DashBoard}></Route>
                            <Route exact path="/background-color" component={BackgroundColorContainer}></Route>
                            <Route exact path="/background-photo" component={BackgroundPhotoContainer}></Route>

                            {process.env.NODE_ENV == 'development' && <DevTools/>}
                        </div>
                    </Router>
                </Provider>
            </ApolloProvider>
        );
    }
}

export default App;
