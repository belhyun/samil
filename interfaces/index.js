import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import $ from 'jquery';
import registerServiceWorker from './registerServiceWorker';
import materialCss from "./assets/css/material-dashboard.css";
//import 'bootstrap/dist/css/bootstrap.min.css';

/**
 * 서비스의 시작점
 */
$(document).ready(function(){
    ReactDOM.render(
        <App />
        , document.getElementById('root')
    );
});

registerServiceWorker();
