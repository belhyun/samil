import { types } from "mobx-state-tree"
import SamilBackgroundStoreModel from "./src/stores/background/SamilBackgroundStoreModel"


export const RootStoreModel =
    types
        .model("RootStore")
        //produces a new type, based on the current one, and adds / overrides the specified properties
        .props({
            // @ts-ignore
            SamilBackgroundStore: types.optional(SamilBackgroundStoreModel, {})
        })
