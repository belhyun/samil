import {flow} from "mobx-state-tree"
import {
    CREATE_SAMIL_BACKGROUND_COLOR,
    GET_ALL_SAMIL_BACKGROUND_COLORS
} from "./SamilBackgroundActinos";
import {SamilApiStateEnumType} from "../../type/SamilApiStateEnum";
import SamilGraphqlClient from "../../graphql/SamilGraphqlClient";
import {SGV} from "../../util/SGV";
import {ISamilBackgroundColor} from "../../stores/background/SamilBackgroundColor";
import http from "../../util/http";

const getAllSamilBackgroundColors = (self: any) =>  {

    return flow(function* () {
        try {
            self.state = SamilApiStateEnumType.FETCHING
            const {data: {allSamilBackgroundColors: allSamilBackgroundColors}} = yield SamilGraphqlClient.query({
                query: GET_ALL_SAMIL_BACKGROUND_COLORS,
                fetchPolicy: "no-cache"
            })
            console.log(allSamilBackgroundColors)
            self.samilBackgroundColors = SGV._.values(allSamilBackgroundColors)
            self.state = SamilApiStateEnumType.DONE
        } catch (err) {
            console.log(err)
            self.state = SamilApiStateEnumType.ERROR
        }
    })
}

const createSamilBackgroundColorAction = (self: any) =>  {

    return flow(function* (samilBackgroundColor: ISamilBackgroundColor, callback: Function) {
        try {
            self.state = SamilApiStateEnumType.FETCHING
            yield SamilGraphqlClient.mutate({
                mutation: CREATE_SAMIL_BACKGROUND_COLOR,
                variables: {
                    input: samilBackgroundColor
                },
                fetchPolicy: "no-cache"
            })
            self.state = SamilApiStateEnumType.DONE
            callback()
        } catch (err) {
            console.log(err)
            self.state = SamilApiStateEnumType.ERROR
        }
    })
}

const createSamilBackgroundPhotoAction = (self: any) =>  {

    return flow(function* (file: File, callback: Function) {
        try {
            const data = new FormData();
            data.append('file', file);
            data.append('filename', file.name);

            self.state = SamilApiStateEnumType.FETCHING
            http.post("samil-background-photo", {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            }, data)
            .catch(function(error){
                return Promise.reject(error);
            })
            .then(http.handleResponse)
            self.state = SamilApiStateEnumType.DONE
            callback()
        } catch (err) {
            console.log(err)
            self.state = SamilApiStateEnumType.ERROR
        }
    })
}

export {
    getAllSamilBackgroundColors,
    createSamilBackgroundColorAction,
    createSamilBackgroundPhotoAction
}




