export const SamilApiStateEnum = [
    "pending",
    "fetching",
    "done",
    "error",
]

export const SamilApiStateEnumType =  {

    "PENDING" : SamilApiStateEnum[0],
    "FETCHING" : SamilApiStateEnum[1],
    "DONE" : SamilApiStateEnum[2],
    "ERROR" : SamilApiStateEnum[3],
}

