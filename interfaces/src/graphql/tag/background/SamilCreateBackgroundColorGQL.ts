import gql from "graphql-tag"

export default gql`
    mutation createSamilBackgroundColor($input: SamilBackgroundColorInput!) {
        createSamilBackgroundColor(input: $input) {
            hex
            red
            green
            blue
            active
        }
    }
`
