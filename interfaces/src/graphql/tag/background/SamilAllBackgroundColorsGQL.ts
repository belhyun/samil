import gql from "graphql-tag"

export default gql`
    {
        allSamilBackgroundColors {
            hex
            red
            green
            blue
        }
    }
`
