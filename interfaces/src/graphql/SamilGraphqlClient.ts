import { ApolloClient } from "apollo-client"
import { createHttpLink } from "apollo-link-http"
import { InMemoryCache } from "apollo-cache-inmemory"

const API_URL = (function() {

    return `http://${process.env.LOCAL_SERVER}:8060/samil-background`

})()

export default new ApolloClient({
    link: createHttpLink({ uri: API_URL }),
    cache: new InMemoryCache({
        resultCaching: false,
    }),
})

