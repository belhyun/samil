import axios from 'axios';
import {SGV} from "./SGV";

const http = {
    post: post,
    get: get,
    // checkResponse: checkResponse,
    handleResponse: handleResponse
};

const commonHeaders = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin':'*'
}

function request(func, baseUrl, headers, body) {

    let localHeaders = {};
    Object.assign(localHeaders, commonHeaders);
    Object.assign(localHeaders, headers);

    return axios
        .create({
            headers: localHeaders
        })[func].call(null, `http://${process.env.LOCAL_SERVER}:8060/`.concat(baseUrl), body);

}

function post(baseUrl, headers, body) {

    return request(
        'post',
        baseUrl,
        SGV._.isUndefined(headers) ? {} : headers,
        SGV._.isUndefined(body) ? {} : body);

}

function get(baseUrl, headers, body) {

    return request(
        'get',
        baseUrl,
        SGV._.isUndefined(headers) ? {} : headers,
        SGV._.isUndefined(body) ? {} : body);
}

// function checkResponse(resp, code) {
//
//     const go = __.go(
//         resp['respCode'],
//         __.all(
//             __.negate(_.isUndefined),
//             __.partial(__.isEqual(code))
//         ),
//         __.args,
//         __.every);
//
//
//    return new Promise(function(resolve, reject) {
//        go ? resolve(resp): reject("response code not match");
//    });
//
//     // Promise.resolve(_.negate(_.isUndefined)(resp['respCode']) && _.isEqual(resp['respCode'], code) && resp);
// }

function handleResponse(response) {
    if (!response) {
        if (response.status === 401) {
            location.reload(true);
        }
        const error = response.statusText;
        return Promise.reject(error);
    }
    return response.data;
}


export default http;
