import {Instance, types} from 'mobx-state-tree';
import {ISamilBackgroundColor, SamilBackgroundColor} from "./SamilBackgroundColor";
import {SGV} from "../../util/SGV";
import {SamilApiStateEnum, SamilApiStateEnumType} from "../../type/SamilApiStateEnum";
import {
    getAllSamilBackgroundColors,
    createSamilBackgroundColorAction,
    createSamilBackgroundPhotoAction
} from "../../actions/background/SamilBackgroundColorActions";


const SamilBackgroundStoreModel = types.model("SamilBackgroundStore", {

    state: types.optional(types.enumeration("State", SamilApiStateEnum), SamilApiStateEnumType.PENDING),
    samilBackgroundColors: types.array(SamilBackgroundColor),

})
// @ts-ignore
    .views( self => ({
    }))
    // @ts-ignore
    .actions(self => {

        // @ts-ignore
        const preparedSelfFunc = (func:Function) => {
            return SGV._.partial(func(self))
        }

        const fetchAllBackgroundColors = function() {
            return preparedSelfFunc(getAllSamilBackgroundColors(self))
        }

        const createSamilBackgroundColor = function(samilBackgroundColor: ISamilBackgroundColor, callback:Function) {
            return createSamilBackgroundColorAction(self)(samilBackgroundColor, callback)
        }

        const createSamilBackgroundPhoto = function(file: File, callback:Function) {
            return createSamilBackgroundPhotoAction(self)(file, callback)
        }

        return {
            createSamilBackgroundColor,
            fetchAllBackgroundColors,
            createSamilBackgroundPhoto
        }
    })

export type SamilBackgroundStore = Instance<typeof SamilBackgroundStoreModel>

export default SamilBackgroundStoreModel
