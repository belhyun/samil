import {Instance, types} from "mobx-state-tree"

export const SamilBackgroundColor = types.model({
    hex: types.optional(types.string, ""),
    red: types.optional(types.number, 0),
    green: types.optional(types.number, 0),
    blue: types.optional(types.number, 0),
    active: types.optional(types.number, 0),
    __typename: types.optional(types.string, ""),
})

export type ISamilBackgroundColor = Instance<typeof SamilBackgroundColor>



