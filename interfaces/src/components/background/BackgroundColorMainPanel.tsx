import React from 'react'
import {SketchPicker} from 'react-color'
import {inject, observer} from "mobx-react"
import {SamilBackgroundStore} from "../../stores/background/SamilBackgroundStoreModel"
import {SamilApiStateEnumType} from "../../type/SamilApiStateEnum"
import {SamilBackgroundColor} from "../../stores/background/SamilBackgroundColor";

interface IBackgroundColorMainPanelProps {
    SamilBackgroundStore: SamilBackgroundStore
}

interface IBackgroundColorMainPanelState {
    hex?: string,
    red?: number,
    green?: number,
    blue?: number,
    active?: number,
    __typename?: string
}

@inject("SamilBackgroundStore")
@observer
class BackgroundColorMainPanel extends React.Component<IBackgroundColorMainPanelProps, IBackgroundColorMainPanelState> {

    public readonly state: Readonly<IBackgroundColorMainPanelState> = {
        hex: "#FFF7AE"
    }

    constructor(props) {
        super(props)
        console.log(this.state)
    }

    componentDidMount() {
        this.props.SamilBackgroundStore.fetchAllBackgroundColors()
    }

    componentDidUpdate() {
    }

    onChangeComplete(color) {
        this.setState({
            hex: color.hex,
            red: color.rgb.r,
            green: color.rgb.g,
            blue: color.rgb.b,
            active: 1,
            __typename: ""
        })
    }

    createSamilBackgroundColor() {
        this.props.SamilBackgroundStore.createSamilBackgroundColor(SamilBackgroundColor.create({
            hex: this.state.hex,
            red: this.state.red,
            green: this.state.green,
            blue: this.state.blue,
            active: 1,
            __typename: ""
        }), () => {
            this.props.SamilBackgroundStore.fetchAllBackgroundColors()
        })
    }

    getBackgroundColorList() {
        if (this.props.SamilBackgroundStore.state === SamilApiStateEnumType.DONE) {
            return (
                <tbody>
                    {this.props.SamilBackgroundStore.samilBackgroundColors.map((color, i) => {
                        return (
                            <tr key={i}>
                                <td>{i+1}</td>
                                <td>{color.hex}</td>
                                <td>{color.red}</td>
                                <td>{color.green}</td>
                                <td>{color.blue}</td>
                            </tr>
                        )
                    })}
                </tbody>
            )
        }
        return (
            <tbody>
            <tr>
                <td>No Record Found</td>
            </tr>
            </tbody>
        )
    }

    render() {
        return (
            <div className="main-panel">
                <div className="content">
                    <div className="container-fluid">
                        <SketchPicker color = {this.state.hex} width={"500px"}height={"500px"} onChangeComplete = {this.onChangeComplete.bind(this)}/>
                        <a href="#pablo" className="btn btn-primary btn-round" onClick={this.createSamilBackgroundColor.bind(this)}>저장하기</a>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card">
                                    <div className="card-header card-header-primary">
                                        <h4 className="card-title ">배경색 리스트</h4>
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                            <table className="table">
                                                <thead className=" text-primary">
                                                <tr>
                                                    <th>
                                                        순서
                                                    </th>
                                                    <th>
                                                        Hex
                                                    </th>
                                                    <th>
                                                        Red
                                                    </th>
                                                    <th>
                                                        Green
                                                    </th>
                                                    <th>
                                                        Blue
                                                    </th>
                                                </tr>
                                                </thead>
                                                {this.getBackgroundColorList()}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BackgroundColorMainPanel;
