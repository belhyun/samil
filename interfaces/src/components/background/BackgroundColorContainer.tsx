import React from 'react';
import SideBar from "../SideBar";
import BackgroundColorMainPanel from "./BackgroundColorMainPanel";
import {inject, observer} from "mobx-react";

@inject("SamilBackgroundStore")
@observer
class BackgroundColorContainer extends React.Component<any, any> {

    constructor(props:any) {
        super(props);
    }

    render() {
        return (
            <div>
                <SideBar active={"BackgroundColorContainer"}/>
                {/*
                    // @ts-ignore */}
                <BackgroundColorMainPanel/>
            </div>
        );
    }
}

export default BackgroundColorContainer
