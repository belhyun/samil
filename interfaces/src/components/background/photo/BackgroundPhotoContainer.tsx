import React from 'react'
import SideBar from "../../SideBar"
import {inject, observer} from "mobx-react"
import BackgroundPhotoMainPanel from "./BackgroundPhotoMainPanel";

@inject("SamilBackgroundStore")
@observer
class BackgroundPhotoContainer extends React.Component<any, any> {

    constructor(props:any) {
        super(props);
    }

    render() {
        return (
            <div>
                <SideBar active={"BackgroundPhotoContainer"}/>
                {/*
                    // @ts-ignore */}
                <BackgroundPhotoMainPanel/>
            </div>
        );
    }
}

export default BackgroundPhotoContainer
