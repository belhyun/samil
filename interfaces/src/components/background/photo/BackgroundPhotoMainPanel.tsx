import React from 'react'
import {inject, observer} from "mobx-react"
import {SamilBackgroundStore} from "../../../stores/background/SamilBackgroundStoreModel";
import Dropzone from 'react-dropzone';

interface IBackgroundColorMainPanelProps {
    SamilBackgroundStore: SamilBackgroundStore
}

interface IBackgroundPhotoMainPanelState {
    dropFiles: Array<File>
}

@inject("SamilBackgroundStore")
@observer
class BackgroundPhotoMainPanel extends React.Component<IBackgroundColorMainPanelProps, IBackgroundPhotoMainPanelState> {

    public readonly state: Readonly<IBackgroundPhotoMainPanelState> = {
        dropFiles : new Array<File>()

    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        // this.props.SamilBackgroundStore.fetchAllBackgroundColors()
    }

    componentDidUpdate() {
    }

    onDrop(dropFiles: Array<File>) {
        this.setState({
            dropFiles: dropFiles
        })
        this.props.SamilBackgroundStore.createSamilBackgroundPhoto(dropFiles[0], () => {})
    }

    render() {
        return (
            <div className="main-panel">
                <div className="content">
                    <div className="container-fluid">
                        <Dropzone onDrop={this.onDrop.bind(this)} accept={["image/jpeg", "image/png"]}>
                            {({getRootProps, getInputProps}) => (
                                <section className="dropzone">
                                    <div {...getRootProps()}>
                                        <input {...getInputProps()} />
                                        <p>파일업로드를 하시면 됩니다.</p>
                                    </div>
                                </section>
                            )}
                        </Dropzone>
                        <ul className="list-group mt-2">
                            {this.state.dropFiles.length > 0 && this.state.dropFiles.map(acceptedFile => (
                                <li key={acceptedFile.name} className="list-group-item list-group-item-success">
                                    {acceptedFile.name}
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default BackgroundPhotoMainPanel;
