import React from 'react';
import tovDashboard from "../../assets/img/tovcard/tov-dashboard.jpg";

class MainPanel extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="main-panel">
                {/* Navbar */}
                <nav className="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div className="container-fluid">
                        <div className="navbar-wrapper">
                            <a className="navbar-brand" href="#pablo"><img src={tovDashboard}></img></a>
                        </div>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="navbar-toggler-icon icon-bar" />
                            <span className="navbar-toggler-icon icon-bar" />
                            <span className="navbar-toggler-icon icon-bar" />
                        </button>
                        <div className="collapse navbar-collapse justify-content-end">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link" href="#pablo">
                                        <i className="material-icons">notifications</i> Notifications
                                    </a>
                                </li>
                                {/* your navbar here */}
                            </ul>
                        </div>
                    </div>
                </nav>
                {/* End Navbar */}
                <div className="content">
                    <div className="container-fluid">
                        {/* your content here */}
                    </div>
                </div>
                <footer className="footer">
                    <div className="container-fluid">
                        <nav className="float-left">
                            <ul>
                                <li>
                                    <a href="https://www.creative-tim.com">
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div className="copyright float-right">
                            ©
                            , made with <i className="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">TovCard</a> for GOD.
                        </div>
                        {/* your footer here */}
                    </div>
                </footer>
            </div>
        );
    }
}

export default MainPanel;
