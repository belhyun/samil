import React from 'react';

class SideBar extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="sidebar" data-color="purple" data-background-color="white">
                {/*
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  */}
                <div className="logo">
                    <a href="/" className="simple-text logo-mini">
                        BELHYUN
                    </a>
                    <a href="/" className="simple-text logo-normal">
                        TovCard Team
                    </a>
                </div>
                <div className="sidebar-wrapper">
                    <ul className="nav">
                        <li className={`nav-item ${_.isUndefined(this.props.active, '')? 'active' : ''}`}>
                            <a className="nav-link" href="/">
                                <i className="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        {/*컬러 선택하기*/}
                        <li className={`nav-item ${_.isEqual(this.props.active, 'BackgroundColorContainer')? 'active' : ''}`}>
                            <a className="nav-link" href="/background-color">
                                <i className="material-icons">library_books</i>
                                <p>백그라운드 컬러 선택관리</p>
                            </a>
                        </li>
                        <li className={`nav-item ${_.isEqual(this.props.active, 'BackgroundPhotoContainer')? 'active' : ''}`}>
                            <a className="nav-link" href="/background-photo">
                                <i className="material-icons">library_books</i>
                                <p>백그라운드 포토 선택관리</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default SideBar;
