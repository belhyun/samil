import React from 'react';
import SideBar from "./SideBar";
import MainPanel from "./MainPanel";

class DashBoard extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="wrapper ">
                <SideBar/>
                <MainPanel/>
            </div>
        );
    }
}

export default DashBoard;
