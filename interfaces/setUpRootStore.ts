import {RootStoreModel} from "./rootStore";


export function setUpRootStore() {

    let rootStore;

    try {

        // @ts-ignore
        rootStore = RootStoreModel.create({});

    } catch (e) {

    }

    return rootStore

}
