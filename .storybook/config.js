import { configure, addDecorator } from '@storybook/react';
import backgrounds from '@storybook/addon-backgrounds';

// addDecorator(
//     backgrounds([
//         { name: "white", value: "#ffffff", default: true },
//         { name: "pink", value: "#ff00ff" },
//         { name: "black", value: "#000000" },
//     ])
// )

const req = require.context(
    '../interfaces/src/components',
    true,
    /\.stories\.(js|jsx)$/,
)

function loadStories() {
    req.keys().forEach(filename => {
        req(filename)
    });
}

configure(loadStories, module);
