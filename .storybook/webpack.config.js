// const path = require('path');
// const webpack = require('webpack');
// const HtmlWebpackPlugin = require('html-webpack-plugin')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
//
// module.exports = {
//     devtool: 'inline-source-map',
//     devServer: {
//         // 어떤 요청이 들어와도 index.html이 보여지도록
//         historyApiFallback: true,
//         // compress: true,
//         publicPath: '/',
//         contentBase: [
//             // path.resolve(__dirname, 'interfaces'),
//             path.resolve(__dirname, '../interfaces', 'bundle'),
//             path.resolve(__dirname, '../node_modules'),
//             path.resolve(__dirname, '../interfaces')
//         ],
//         host: "localhost",
//         hot: true,
//         inline: true,
//         port: 4000
//         // proxy: {
//         //     "**": {
//         //         "changeOrigin": true,
//         //         "secure": false
//         //     }
//         // }
//     },
//     entry: path.resolve(__dirname, '../interfaces', 'index.js'),
//     output: {
//         path: path.resolve(__dirname, '../interfaces', 'bundle'),
//         filename: 'bundle.js'
//     },
//     module: {
//         rules:[
//             {
//                 test: /\.(css|less)$/,
//                 use:['style-loader','css-loader']
//             },
//             {
//                 test: /\.(png|jpg|gif)$/,
//                 use: [
//                     {
//                         loader: 'file-loader?name=/bundle/[name].[ext]',
//                         options: {}
//                     }
//                 ]
//             },
//             {
//                 test: /\.js$/ ,
//                 exclude: '/node_modules/',
//                 use: [
//                     {
//                         loader: 'babel-loader'
//                     },
//                 ],
//             },
//             {
//                 test: /\.jsx$/ ,
//                 exclude: '/node_modules/',
//                 use: [
//                     {
//                         loader: 'babel-loader'
//                     },
//                 ]
//             },
//             {
//                 test: /\.svg$/,
//                 use: [
//                     {
//                         loader: 'file-loader',
//                         options: {
//                             name: '[name].[ext]?[hash]',
//                             publicPath: './bundle/'
//                         }
//                     }
//                 ]
//             },
//             {
//                 test: /\.tsx?$/,
//                 use: 'ts-loader',
//                 exclude: /node_modules/
//             }
//         ]
//     },
//     resolve: {
//         extensions: [ '.tsx', '.ts', '.js' ]
//     },
//     plugins: [
//         // The EnvironmentPlugin accepts either an array of keys or an object mapping its keys to their default values.
//         new webpack.EnvironmentPlugin({
//             NODE_ENV: 'dev', // use 'development' unless process.env.NODE_ENV is defined
//             DEBUG: true
//         }),
//         new webpack.NamedModulesPlugin(), //prints more readable module names in the browser console on HMR updates
//         new webpack.DefinePlugin({
//             'process.env': {
//                 NODE_ENV: JSON.stringify('dev')
//             }
//         }),
//         new webpack.HotModuleReplacementPlugin(),
//         new webpack.ProvidePlugin({
//             _: 'underscore',
//             _s: 'underscore.string',
//             __: 'partial-js',
//             Modernizr: "modernizr"
//         }),//,
//         new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ko/),
//         //new RemoveStrictPlugin()
//         //,
//         //new UglifyJsPlugin()
//         //,
//         // new HtmlWebpackPlugin({
//         //     template: "../interfaces/index.html",
//         //     filename: "../interfaces/bundle/index.html",
//         //     inject: false
//         // })
//     ],
//     cache: true
//     // output: {
//     //     pathinfo: false
//     // }
// }
const path = require('path');


module.exports = (baseConfig, env, defaultConfig) => {

    defaultConfig.module.rules.push({
        test: /\.(ts|tsx)$/,
        include: path.resolve(__dirname, "../interfaces/src"),
        loader: require.resolve("ts-loader")
    });
    defaultConfig.resolve.extensions.push(".ts", ".tsx");

    return defaultConfig;
};