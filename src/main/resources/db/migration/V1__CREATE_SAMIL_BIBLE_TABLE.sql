
CREATE TABLE `samil_bible` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(25) DEFAULT NULL,
  `version` varchar(25) DEFAULT NULL,
  `word_ref` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `chapter` varchar(250) DEFAULT NULL,
  `verse` varchar(250) DEFAULT NULL,
  `word` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

