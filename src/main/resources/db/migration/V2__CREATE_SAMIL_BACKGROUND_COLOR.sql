CREATE TABLE `samil_background_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hex` varchar(25) DEFAULT NULL,
  `red` int(11) DEFAULT 0,
  `green` int(11) DEFAULT 0,
  `blue` int(11) DEFAULT 0,
  `active` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
