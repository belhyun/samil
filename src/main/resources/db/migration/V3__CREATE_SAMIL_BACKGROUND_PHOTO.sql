CREATE TABLE `samil_background_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(25) DEFAULT NULL,
  `mime_type` varchar(100) DEFAULT NULL,
  `etag` varchar(500) DEFAULT 0,
  `s3uri` varchar(500) DEFAULT 0,
  `active` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
