package com.belhyun.bible.samil.aws.s3;


import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.belhyun.bible.samil.util.property.SamilProperties;
import com.google.common.io.Files;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
@Component
@Slf4j
public class SamilS3Uploader {

    @Autowired
    private AmazonS3Client amazonS3Client;

    @Autowired
    private SamilProperties samilProperties;

    private String bucket;

    public SamilS3UploadDTO upload(MultipartFile multipartFile, String dirName, int seq) throws IOException {
        File uploadFile = convert(multipartFile, seq)
                .orElseThrow(() -> new IllegalArgumentException("MultipartFile -> File로 전환이 실패했습니다."));

        return upload(uploadFile, dirName, seq);
    }

    private SamilS3UploadDTO upload(File uploadFile, String dirName, int seq) {
        String fileName = dirName + "/" + seq + "/" + uploadFile.getName();
        String uploadImageUrl = putS3(uploadFile, fileName);
        removeNewFile(uploadFile);
        return SamilS3UploadDTO.builder()
                .uploadFileName(fileName)
                .uploadImageUrl(uploadImageUrl).build();
    }

    private String putS3(File uploadFile, String fileName) {
        amazonS3Client.putObject(new PutObjectRequest(this.bucket, fileName, uploadFile).withCannedAcl(CannedAccessControlList.PublicRead));
        return amazonS3Client.getUrl(this.bucket, fileName).toString();
    }

    @PostConstruct
    public void setBucketValue() {
        this.bucket = samilProperties.getValue("aws.s3.bucket");
    }

    private void removeNewFile(File targetFile) {
        if (targetFile.delete()) {
            log.info("파일이 삭제되었습니다.");
        } else {
            log.info("파일이 삭제되지 못했습니다.");
        }
    }

    private Optional<File> convert(MultipartFile file, int seq) throws IOException {
        File convertFile = new File(seq + "." + Files.getFileExtension(file.getOriginalFilename()));
        if (convertFile != null) {
            try (FileOutputStream fos = new FileOutputStream(convertFile)) {
                fos.write(file.getBytes());
            }
            return Optional.of(convertFile);
        }
        return Optional.empty();
    }
}
