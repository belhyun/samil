package com.belhyun.bible.samil.aws.s3;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SamilS3UploadDTO {

    private String uploadFileName;

    private String uploadImageUrl;
}
