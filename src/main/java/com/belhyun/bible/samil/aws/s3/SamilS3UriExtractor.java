package com.belhyun.bible.samil.aws.s3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SamilS3UriExtractor {

    private static String SMALL_PATH = "resized/small/resized-";

    public static String GET_SMALL_S3URI_PATH(String originPath) {

        Pattern pattern = Pattern.compile("(\\w+)(\\.\\w+)+(?!.*(\\w+)(\\.\\w+)+)");
        Matcher matcher = pattern.matcher(originPath);

        String result = "";

        while (matcher.find()) {
            result = matcher.replaceFirst(SMALL_PATH + matcher.group(1) + matcher.group(2));
        }
        return result;
    }
}
