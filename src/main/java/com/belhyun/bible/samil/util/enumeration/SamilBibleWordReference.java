package com.belhyun.bible.samil.util.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.EnumSet;

@Getter
@AllArgsConstructor
public enum SamilBibleWordReference implements SamilEnumInterface {

    EMPTY("무", "찾지못함"),
    GENESIS("창","창세기"),
    EXODUS("출","출애급기"),
    LEVITICUS("레","레위기"),
    NUMBERS("민","민수기"),
    DEUTERONOMY("신","신명기"),
    JOSHUA("수","여호수아"),
    JUDGES("삿","사사기"),
    RUTH("롯","롯기"),
    SAMUEL_1("삼상","사무엘상"),
    SAMUEL_2("삼하","사무엘하"),
    KINGS_1("왕상","열왕기상"),
    KINGS_2("왕하","열왕기하"),
    CHRONICLES_1("대상","역대상"),
    CHRONICLES_2("대하","역대하"),
    EZRA("스","에스라"),
    NEHEMIA("느","느헤미야"),
    ESTHER("에","에스더"),
    JOB("욥","욥기"),
    PSALMS("시","시편"),
    PROVERBS("잠","잠언"),
    ECCLESIASTES("전","전도서"),
    SONG_OF_SOLOMON("아","아가"),
    ISAIAH("사","이사야"),
    JEREMIAH("렘","예레미야"),
    LAMENTATIONS("애","예레미야애가"),
    EZEKIEL("겔","에스겔"),
    DANIEL("단","다니엘"),
    HOSEA("호","호세아"),
    JOEL("욜","요엘"),
    AMOS("암","아모스"),
    OBADIAH("옵","오바댜"),
    JONAH("욘","요나"),
    MICAH("미","미가"),
    NAHUM("나","나훔"),
    HABAKKUK("합","하박국"),
    ZEPHANIAH("습","스바냐"),
    HAGGAI("학","학개"),
    ZECHARIAH("슥","스가랴"),
    MALACHI("말","말라기"),
    MATTHEW("마","마태복음"),
    MARK("막","마가복음"),
    LUKE("눅","누가복음"),
    JOHN("요","요한복음"),
    ACTS("행","사도행전"),
    ROMANS("롬","로마서"),
    CORINTHIANS_1("고전","고린도전서"),
    CORINTHIANS_2("고후","고린도후서"),
    GALATIANS("갈","갈라디아서"),
    EPHESIANS("엡","에베소서"),
    PHILIPPIANS("빌","빌립보서"),
    COLOSSIANS("골","골로새서"),
    THESSALONIANS_1("살전","데살로니가전서"),
    THESSALONIANS_2("살후","데실로니가후서"),
    TIMOTHY_1("딤전","디모데전서"),
    TIMOTHY_2("딤후","디모데후서"),
    TITUS("딛","디도서"),
    PHILEMON("몬","빌레몬서"),
    HEBREWS("히","히브리서"),
    JAMES("약","야고보서"),
    PETER_1("벧전","베드로전서"),
    PETER_2("벧후","베드로후서"),
    JOHN_1("요일","요한1서"),
    JOHN_2("요이","요한2서"),
    JOHN_3("요삼","요한3서"),
    JUDE("유","유다서"),
    REVELATION("계","요한게시록");


    private String message;

    private String description;


    public static SamilBibleWordReference getBibleName(String workdRef) {

        return EnumSet.allOf(SamilBibleWordReference.class).stream()
                .filter(e -> e.getMessage().equals(workdRef))
                .findFirst().orElse(EMPTY);

    }

}
