package com.belhyun.bible.samil.util.property;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SamilProperties {

    @Autowired
    private Environment environment;

    public String getValue(String key) {
        return environment.getProperty(key);
    }
}
