package com.belhyun.bible.samil.util.enumeration;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SamilBibleLangEnum implements SamilEnumInterface {

    KR("KR", "한국어"),

    EN("EN", "영어");

    private String message;

    private String description;

}
