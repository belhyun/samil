package com.belhyun.bible.samil.util.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SamilRestResponse<T> {

    private String message;

    private SamilRestResponseCode respCode;

    private T body;

    public static <T> SamilRestResponse<T> make(String message, SamilRestResponseCode code, T body) {
        SamilRestResponse<T> restResponse = new SamilRestResponse<>(message, code, body);
        return restResponse;
    }
}
