package com.belhyun.bible.samil.util.file;


import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class SamilFileReader {

    public static Optional<String> read(String filePath) {

        try {
            return Optional.of(new String(Files.readAllBytes(Paths.get(filePath))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static String readFromReader(Reader reader) throws IOException {
        char[] buffer = new char[1024 * 4];
        StringWriter sw = new StringWriter();
        int n;
        while (-1 != (n = reader.read(buffer))) {
            sw.write(buffer, 0, n);
        }
        return sw.toString();
    }
}
