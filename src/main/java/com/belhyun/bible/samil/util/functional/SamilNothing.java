package com.belhyun.bible.samil.util.functional;

@FunctionalInterface
public interface SamilNothing {

    void apply();
}
