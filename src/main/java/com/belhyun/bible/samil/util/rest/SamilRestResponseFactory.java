package com.belhyun.bible.samil.util.rest;


import java.util.List;
import java.util.stream.Collectors;

public class SamilRestResponseFactory<T> {

    private static SamilRestResponseFactory factory;

    public static <T> SamilRestResponseFactory getInstance() {

        if (factory == null) {
            factory = new SamilRestResponseFactory<T>();
        }
        return factory;
    }

    public SamilRestResponse<T> success(SamilRestDtoMaker<T> from, SamilRestResponseCode code) {
        return SamilRestResponse.make(
                code.getMessage(),
                code,
                from.makeDto()
        );
    }

    public SamilRestResponse<List<T>> success(List<SamilRestDtoMaker<T>> from, SamilRestResponseCode code) {


        return SamilRestResponse.make(
                code.getMessage(),
                code,
                from.stream().map((in) -> in.makeDto()).collect(Collectors.toList()));

    }
}
