package com.belhyun.bible.samil.util.enumeration;

public interface SamilEnumInterface {

    String getMessage();

    String getDescription();

}
