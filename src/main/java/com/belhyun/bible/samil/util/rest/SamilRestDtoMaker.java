package com.belhyun.bible.samil.util.rest;

public interface SamilRestDtoMaker<T> {

    T makeDto();
}
