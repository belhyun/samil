package com.belhyun.bible.samil.util.rest;

public interface SamilRestResponseCode {

    String getMessage();
}
