package com.belhyun.bible.samil.controller.bible;

import com.belhyun.bible.samil.graphql.base.SamilGraphqlProvider;
import com.belhyun.bible.samil.graphql.util.SamilExecutionInput;
import com.belhyun.bible.samil.graphql.util.SamilExecutionInputSet;
import graphql.ExecutionResult;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/samil-bibles")
public class SamilBibleController {

    @Resource(name = "SamilBibleGraphqlProvider")
    private SamilGraphqlProvider samilGraphqlProvider;


    @PostMapping
    public ResponseEntity<Object> getSamilBibleByQuery(@RequestBody String query) throws JSONException {

        SamilExecutionInputSet samilExecutionInputSet = SamilExecutionInput.make(query);

        ExecutionResult result = null;
        if (samilExecutionInputSet.isQuery()) {
            result = samilGraphqlProvider.execute(samilExecutionInputSet.getQuery());
        }

        if (samilExecutionInputSet.isExecutionInput()) {
            result = samilGraphqlProvider.execute(samilExecutionInputSet.getInput());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
