package com.belhyun.bible.samil.controller.bible.search;


import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.google.common.collect.Lists;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SamilBibleGroupByNameDTO {


    /**
     * 그룹아이디
     */
    private int groupId= 0;

    private String chapter;

    /**
     * 성경버젼
     */
    private String version = "";

    /**
     * 성경이름
     */
    private String name = "";

    /**
     * 챕터에 포함된 성경의 개수
     */
    private int count = 0;

    /**
     * 챕터에 포함된 성경 리스트
     */
    private List<SamilBible> samilBibles = Lists.newArrayList();


    public void addSamilBibleIfNameMatch(SamilBible samilBible) {

        if (samilBible.getName().equals(name)) {
            this.count++;
            samilBibles.add(samilBible);
        }
    }
}
