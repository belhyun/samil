package com.belhyun.bible.samil.controller.health;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health-check")
public class SamilHealthController {

    @GetMapping
    public String success() {
        return "success";
    }
}
