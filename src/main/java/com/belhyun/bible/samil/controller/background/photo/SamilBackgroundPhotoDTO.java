package com.belhyun.bible.samil.controller.background.photo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SamilBackgroundPhotoDTO {

    private String s3Uri;

    private String smallS3Uri;

    private String mimeType;

}
