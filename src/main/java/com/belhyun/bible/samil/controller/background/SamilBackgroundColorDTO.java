package com.belhyun.bible.samil.controller.background;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class SamilBackgroundColorDTO {

    /**
     * hex 값
     */
    private String hex;

    /**
     * red
     */

    private int red;

    /**
     * green
     */
    private int green;

    /**
     * blue
     */
    private int blue;

    /**
     * active
     */
    private int active;

}
