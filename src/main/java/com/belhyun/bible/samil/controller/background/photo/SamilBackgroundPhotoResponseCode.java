package com.belhyun.bible.samil.controller.background.photo;

import com.belhyun.bible.samil.util.rest.SamilRestResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SamilBackgroundPhotoResponseCode implements SamilRestResponseCode {

    SUCCESS("성공");

    private String message;
}
