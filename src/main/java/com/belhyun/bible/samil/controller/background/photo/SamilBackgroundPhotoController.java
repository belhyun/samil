package com.belhyun.bible.samil.controller.background.photo;

import com.belhyun.bible.samil.domain.background.photo.SamilBackgroundPhotoService;
import com.belhyun.bible.samil.util.rest.SamilRestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin
@ResponseBody
public class SamilBackgroundPhotoController {

    @Autowired
    private SamilBackgroundPhotoService photoService;

    @ResponseBody
    @PostMapping(value = "/samil-background-photo")
    public SamilRestResponse<SamilBackgroundPhotoDTO> dispatcher(@RequestParam("file") MultipartFile file) {
        return photoService.upload(file);
    }

    @ResponseBody
    @GetMapping(value = "/get-samil-background-photo")
    public SamilRestResponse<List<SamilBackgroundPhotoDTO>> get() {
        return photoService.getSamilBackgroundPhotos();
    }
}
