package com.belhyun.bible.samil.controller.background;


import com.belhyun.bible.samil.graphql.base.SamilGraphqlProvider;
import com.belhyun.bible.samil.graphql.util.SamilExecutionInput;
import com.belhyun.bible.samil.graphql.util.SamilExecutionInputSet;
import graphql.ExecutionResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/samil-background")
public class SamilBackgroundController {

    @Resource(name = "SamilBackgroundGraphqlProvider")
    private SamilGraphqlProvider samilGraphqlProvider;

    @PostMapping
    public ResponseEntity<Object> dispatcher(@RequestBody String query) {

        ExecutionResult result = null;

        SamilExecutionInputSet samilExecutionInputSet = SamilExecutionInput.make(query);


        if (samilExecutionInputSet.isQuery()) {
            result = samilGraphqlProvider.execute(samilExecutionInputSet.getQuery());
        }
        if (samilExecutionInputSet.isExecutionInput()) {
            result = samilGraphqlProvider.execute(samilExecutionInputSet.getInput());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
