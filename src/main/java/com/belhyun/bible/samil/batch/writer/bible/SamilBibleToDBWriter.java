package com.belhyun.bible.samil.batch.writer.bible;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.domain.bible.repository.SamilBibleRepository;
import com.belhyun.bible.samil.util.cons.SamilComplement;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.batch.api.listener.StepListener;
import javax.batch.runtime.StepExecution;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Component
public class SamilBibleToDBWriter implements ItemWriter<SamilBible>, StepListener {

    private StepExecution stepExecution;

    @Autowired
    private SamilBibleRepository repository;

    @Override
    @Transactional
    public void write(List<? extends SamilBible> items) {

        SamilBible samilBible = items.get(0);

        Optional<SamilBible> fetchedOne = repository.findByChapterAndVerseAndVersionAndLang(samilBible.getChapter(),
                samilBible.getVerse(), samilBible.getVersion(), samilBible.getLang(), samilBible.getName());

        if (SamilComplement.NOT(fetchedOne.isPresent())) {
            repository.save(samilBible);
        }
    }

    @Override
    public void beforeStep() throws Exception {
        this.stepExecution = stepExecution;
    }

    @Override
    public void afterStep() throws Exception {

    }

}
