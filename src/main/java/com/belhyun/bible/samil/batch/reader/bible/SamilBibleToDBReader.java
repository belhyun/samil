package com.belhyun.bible.samil.batch.reader.bible;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.domain.bible.reader.SamilBibleReadService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

import static com.belhyun.bible.samil.util.functional.SamilEquals.intIsEqual;

@Component
@Slf4j
public class SamilBibleToDBReader implements ItemReader<SamilBible> {

    @Resource(name = "SamilKrBibleReadServiceImpl")
    private SamilBibleReadService samilKrBibleReadServiceImpl;

    @Resource(name = "SamilEnBibleReadServiceImpl")
    private SamilBibleReadService samilEnBibleReadServiceImpl;

    @Autowired
    private ApplicationContext applicationContext;


    private int idx = 0;


    private List<SamilBible> target = Lists.newArrayList();


    @Override
    public SamilBible read() throws IOException {


        if (target.isEmpty()) {

            target.addAll(samilKrBibleReadServiceImpl.read());

            //target.addAll(samilEnBibleReadServiceImpl.read());
        }

        if (intIsEqual(target.size(), idx)) {
            return null;
        }

        log.info("SamilBibleToDBReader::read 처리아이템 {}", target.get(idx).toString());

        return target.get(idx++);
    }
}
