package com.belhyun.bible.samil.batch.processor.bible;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class SamilBibleToDBProcessor implements ItemProcessor<SamilBible, SamilBible> {

    @Override
    public SamilBible process(SamilBible item) throws Exception {

        return item;
    }
}
