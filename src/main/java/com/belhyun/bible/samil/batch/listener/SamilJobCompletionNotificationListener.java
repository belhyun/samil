package com.belhyun.bible.samil.batch.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SamilJobCompletionNotificationListener extends JobExecutionListenerSupport {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void afterJob(JobExecution jobExecution) {
        super.afterJob(jobExecution);

        jobExecution.setExitStatus(ExitStatus.COMPLETED);

        System.exit(0);
    }
}
