package com.belhyun.bible.samil.batch.configuration;

import com.belhyun.bible.samil.batch.listener.SamilJobCompletionNotificationListener;
import com.belhyun.bible.samil.batch.processor.bible.SamilBibleToDBProcessor;
import com.belhyun.bible.samil.batch.reader.bible.SamilBibleToDBReader;
import com.belhyun.bible.samil.batch.writer.bible.SamilBibleToDBWriter;
import com.belhyun.bible.samil.domain.bible.SamilBible;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Configuration
@EnableBatchProcessing
public class SamilBatchJobConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private SamilBibleToDBReader samilBibleToDBReader;

    @Autowired
    private SamilBibleToDBProcessor samilBibleToDBProcessor;

    @Autowired
    private SamilBibleToDBWriter samilBibleToDBWriter;

    @Bean
    public SamilBibleToDBProcessor processor() {
        return new SamilBibleToDBProcessor();
    }


    @Bean
    public Job Job(SamilJobCompletionNotificationListener listener) {
        return jobBuilderFactory.get("Job1")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(Step1())
                .end()
                .build();
    }


    @Bean
    public Step Step1() {
        return stepBuilderFactory.get("Step1")
                .<SamilBible, SamilBible>chunk(1)
                .reader(samilBibleToDBReader)
                .processor(samilBibleToDBProcessor)
                .writer(samilBibleToDBWriter)
                .build();

    }

}
