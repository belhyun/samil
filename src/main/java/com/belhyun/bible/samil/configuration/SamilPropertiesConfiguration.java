package com.belhyun.bible.samil.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

@Configuration
@Slf4j
public class SamilPropertiesConfiguration extends PropertyPlaceholderConfigurer implements EnvironmentAware, InitializingBean {

    @Autowired
    private ResourceLoader resourceLoader;

    private Environment environment;

    private static final String PROPERTY_LOCATION_KEY = "application.properties.location";

    private String[] locations;

    @Override
    public void afterPropertiesSet() throws Exception {

        //프로퍼티 소스를 가져옴
        MutablePropertySources envPropSources = ((ConfigurableEnvironment) this.environment).getPropertySources();

        envPropSources.forEach(propertySource -> {
            //정의한 yaml 프로퍼티 로케이션이 존재한다면
            if (propertySource.containsProperty(PROPERTY_LOCATION_KEY)) {
                locations = ((String) propertySource.getProperty(PROPERTY_LOCATION_KEY)).split(",");
                //로드
                stream(locations).forEach(filename -> loadProperties(filename).forEach(source ->{
                    envPropSources.addFirst(source);
                }));
            }
        });
    }

    private List<PropertySource> loadProperties(final String filename) {
        YamlPropertySourceLoader loader = new YamlPropertySourceLoader();
        try {
            final Resource[] possiblePropertiesResources = ResourcePatternUtils.
                    getResourcePatternResolver(resourceLoader).getResources(filename);
            return stream(possiblePropertiesResources)
                    .filter(Resource::exists)
                    .map(resource1 -> {
                        try {
                            return loader.load(resource1.getFilename(), resource1);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }).flatMap(l -> l.stream())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
