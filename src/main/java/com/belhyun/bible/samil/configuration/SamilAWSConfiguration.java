package com.belhyun.bible.samil.configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.belhyun.bible.samil.util.property.SamilProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class SamilAWSConfiguration {

    @Autowired
    private SamilProperties samilProperties;

    @Bean
    public AmazonS3Client amazonS3Client() {
        BasicAWSCredentials awsCredentials =
                new BasicAWSCredentials(samilProperties.getValue("aws.accessKey"), samilProperties.getValue("aws.secretKey"));
        return (AmazonS3Client) AmazonS3ClientBuilder.standard()
                .withRegion(samilProperties.getValue("aws.s3.region.static"))
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }
}
