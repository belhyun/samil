package com.belhyun.bible.samil.graphql.background.color;

import com.belhyun.bible.samil.controller.background.SamilBackgroundColorDTO;
import com.belhyun.bible.samil.domain.background.SamilBackgroundRepositoryProvider;
import com.belhyun.bible.samil.domain.background.color.SamilBackgroundColor;
import com.belhyun.bible.samil.graphql.base.SamilGraphQlDataFetcherRoute;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcher;
import com.belhyun.bible.samil.util.checker.SamilNullChecker;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class SamilBackgroundColorMutationDataFetcher extends SamilGraphqlDataFetcher<SamilBackgroundColorDTO> {

    @Autowired
    private SamilBackgroundRepositoryProvider provider;

    private static String INPUT_ARG_KEY = "input";


    @Override
    public SamilBackgroundColorDTO get(DataFetchingEnvironment dataFetchingEnvironment) {

        SamilBackgroundColorDTO result = SamilBackgroundColorDTO.builder().build();

        final SamilGraphQlDataFetcherRoute key =
                SamilGraphQlDataFetcherRoute.getByKey(dataFetchingEnvironment.getExecutionContext().getOperationDefinition().getName());

        switch(key) {
            case SETTING_BACKGROUND_COLOR:
                result = settingBackGroundColor(dataFetchingEnvironment);
        }
        return result;

    }

    private SamilBackgroundColorDTO settingBackGroundColor(DataFetchingEnvironment dataFetchingEnvironment) {

        Map<String, Object> arguments = dataFetchingEnvironment.getArgument(INPUT_ARG_KEY);

        Optional<SamilBackgroundColor> byHex = provider.getSamilBackgroundColorRepository().getByHex(String.valueOf(arguments.get("hex")));

        if (byHex.isPresent()) {
            return byHex.get().toDTO();
        }

        SamilBackgroundColor save = provider.getSamilBackgroundColorRepository().save(SamilBackgroundColor.builder()
                .hex(String.valueOf(arguments.get("hex")))
                .red((Integer) arguments.get("red"))
                .blue((Integer) arguments.get("blue"))
                .green((Integer) arguments.get("green"))
                .active((Integer) arguments.get("active")).build());

        if (SamilNullChecker.isNotNull(save)) {
            return save.toDTO();
        }
        return SamilBackgroundColor.builder().build().toDTO();
    }
}
