package com.belhyun.bible.samil.graphql.base;

import com.belhyun.bible.samil.graphql.background.color.SamilBackgroundColorDataFetcher;
import com.belhyun.bible.samil.graphql.background.color.SamilBackgroundColorMutationDataFetcher;
import com.belhyun.bible.samil.graphql.bible.AllSamilBibleDataFetcher;
import com.belhyun.bible.samil.graphql.bible.SamilBibleSearchByNameFetcher;
import com.belhyun.bible.samil.graphql.bible.SamilBibleSearchDataFetcher;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Predicate;

@Getter
@AllArgsConstructor
public enum SamilGraphQlDataFetcherRoute {

    ALL_SAMIL_BIBLES("allSamilBibles", AllSamilBibleDataFetcher.class, SamilGraphqlDataFetcherType.QUERY),
    SEARCH_SAMIL_BIBLES("searchSamilBibles", SamilBibleSearchDataFetcher.class, SamilGraphqlDataFetcherType.QUERY),
    SEARCH_SAMIL_BIBLES_BY_CHAPTER("searchSamilBiblesGroupByName", SamilBibleSearchByNameFetcher.class, SamilGraphqlDataFetcherType.QUERY),
    SETTING_BACKGROUND_COLOR("createSamilBackgroundColor", SamilBackgroundColorMutationDataFetcher.class, SamilGraphqlDataFetcherType.MUTATION),
    ALL_SAMIL_BACKGROUND_COLORS("allSamilBackgroundColors", SamilBackgroundColorDataFetcher.class, SamilGraphqlDataFetcherType.QUERY);

    private String route;
    private Class<? extends SamilGraphqlDataFetcher> clazz;
    private SamilGraphqlDataFetcherType fetcherType;

    public static SamilGraphQlDataFetcherRoute getByClazz(Class<? extends SamilGraphqlDataFetcher> clazz) {

        return getByAny((p) -> p.getClazz().equals(clazz), ALL_SAMIL_BIBLES);

    }

    public static SamilGraphQlDataFetcherRoute getByKey(String route) {

        return getByAny((p) -> p.getRoute().equals(route), ALL_SAMIL_BIBLES);

    }


    private static SamilGraphQlDataFetcherRoute getByAny(Predicate<SamilGraphQlDataFetcherRoute> predicateFunc,
                                                         SamilGraphQlDataFetcherRoute defaultRoute) {

        return Lists.newArrayList(SamilGraphQlDataFetcherRoute.values()).stream()
                .filter(p -> predicateFunc.test(p)).findFirst().orElse(defaultRoute);

    }

}
