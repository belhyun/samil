package com.belhyun.bible.samil.graphql.base;

import com.belhyun.bible.samil.graphql.base.type.SamilGraphqlRuntimeTypeBinder;
import com.google.common.collect.Lists;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;

@Component
public class SamilGraphqlSchemaLoader {

    private static String CLASS_PATH_OF_GRAPHQL = "classpath:/graphql/";

    @Autowired
    private ResourcePatternResolver resourcePatternResolver;

    public Optional<GraphQL> load(String resourceName, List<SamilGraphqlDataFetcherSet> dataFetcherList)  {

        try {
            Resource resource =
                    Lists.newArrayList(resourcePatternResolver.getResources(CLASS_PATH_OF_GRAPHQL + resourceName)).get(0);

            TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(new InputStreamReader(resource.getInputStream()));

            RuntimeWiring.Builder builder = RuntimeWiring.newRuntimeWiring();

            builder = SamilGraphqlRuntimeTypeBinder.bind(
                    SamilGraphqlDataFetcherType.MUTATION,
                    SamilGraphqlRuntimeTypeBinder.bind(SamilGraphqlDataFetcherType.QUERY, builder, dataFetcherList),
                    dataFetcherList);


            GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeDefinitionRegistry, builder.build());

            return Optional.of(GraphQL.newGraphQL(schema).build());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


}
