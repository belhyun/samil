package com.belhyun.bible.samil.graphql.util;


import graphql.ExecutionInput;
import lombok.Data;

@Data
public class SamilExecutionInputSet {

    /**
     * QUERY
     * EXECUTION_INPUT
     */
    private static final String QUERY = "QUERY";
    private static final String EXECUTION_INPUT = "EXECUTION_INPUT";
    private String requestType;

    private ExecutionInput input;

    private String query;

    public boolean isQuery() {
        return QUERY.equals(requestType);
    }

    public boolean isExecutionInput() {
        return EXECUTION_INPUT.equals(requestType);
    }

    public static SamilExecutionInputSet makeQuerySet(String query) {

        SamilExecutionInputSet set = new SamilExecutionInputSet();

        set.requestType = QUERY;
        set.query = query;

        return set;
    }

    public static SamilExecutionInputSet makeExecutionInputSet(ExecutionInput input) {

        SamilExecutionInputSet set = new SamilExecutionInputSet();

        set.requestType = EXECUTION_INPUT;
        set.input = input;

        return set;
    }
}
