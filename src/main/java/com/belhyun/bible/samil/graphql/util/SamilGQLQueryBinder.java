package com.belhyun.bible.samil.graphql.util;

import com.belhyun.bible.samil.graphql.SamilGQLQueryDTO;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SamilGQLQueryBinder {

    public static SamilGQLQueryDTO parse(String query) {

        SamilGQLQueryDTO dto = new SamilGQLQueryDTO();

        try {
            JSONObject object = new JSONObject(query);
            JSONObject variables = object.getJSONObject("variables");

            Iterator keys = variables.keys();

            while(keys.hasNext()) {
                String key = String.valueOf(keys.next());
                dto.getVariables().put(key, String.valueOf(variables.get(key)));
            }

            String queryStr = object.getString("query");

            dto.setQuery(dto.getVariables().entrySet().stream().reduce(queryStr,
                    (initial, entry) -> initial.replaceAll("\\$" + entry.getKey(), "\"" + entry.getValue() + "\""),
                    (a, b) -> a));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dto;
    }

    public static String getOperationNameFromQuery(String query) {

        String operationName = null;

        Pattern pattern = Pattern.compile("\\{\\s+(\\w+)\\(");

        Matcher matcher = pattern.matcher(query);

        while(matcher.find()) {
            operationName = matcher.group(1);
        }

        return operationName;
    }
}
