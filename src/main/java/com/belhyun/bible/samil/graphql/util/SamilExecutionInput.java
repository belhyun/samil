package com.belhyun.bible.samil.graphql.util;

import com.belhyun.bible.samil.util.checker.SamilNullChecker;
import com.belhyun.bible.samil.util.cons.SamilComplement;
import com.google.gson.Gson;
import graphql.ExecutionInput;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.HashMap;

public class SamilExecutionInput {

    public static SamilExecutionInputSet make(String query) {

        SamilNullChecker.isNotNull(query);

        ExecutionInput.Builder builder = ExecutionInput.newExecutionInput();

        try {
            JSONObject queryJsonObj = new JSONObject(query);

            /**
             * 액션명
             */
            if (queryJsonObj.isNull("operationName")) {
                builder.operationName(SamilGQLQueryBinder.getOperationNameFromQuery(queryJsonObj.get("query").toString()));
            } else {
                builder.operationName(queryJsonObj.get("operationName").toString());
            }
            /**
             * 쿼리
             */
            builder.query(queryJsonObj.get("query").toString());
            /**
             * 파라미터
             */
            if (SamilComplement.NOT(queryJsonObj.isNull("variables"))) {
                builder.variables(new Gson().fromJson(queryJsonObj.get("variables").toString(), HashMap.class));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return SamilExecutionInputSet.makeExecutionInputSet(builder.build());
    }
}
