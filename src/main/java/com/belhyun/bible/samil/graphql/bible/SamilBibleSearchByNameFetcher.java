package com.belhyun.bible.samil.graphql.bible;

import com.belhyun.bible.samil.controller.bible.search.SamilBibleGroupByNameDTO;
import com.belhyun.bible.samil.domain.bible.services.SamilBibleService;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcher;
import com.belhyun.bible.samil.util.constants.SamilString;
import com.google.common.base.Preconditions;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SamilBibleSearchByNameFetcher extends SamilGraphqlDataFetcher<List<SamilBibleGroupByNameDTO>> {

    @Autowired
    private SamilBibleService samilBibleService;

    @Override
    public List<SamilBibleGroupByNameDTO> get(DataFetchingEnvironment environment) {

        Preconditions.checkNotNull(environment.getArgument("query"));

        return samilBibleService.searchBiblesGroupByName(
                        SamilString.EMPTY_STRING,
                        environment.getArgument("query")).get();

    }
}
