package com.belhyun.bible.samil.graphql;

import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SamilGQLQueryDTO {

    private Map<String, String> variables = Maps.newHashMap();

    private String query;


}
