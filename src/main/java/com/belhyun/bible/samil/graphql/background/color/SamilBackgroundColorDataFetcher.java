package com.belhyun.bible.samil.graphql.background.color;

import com.belhyun.bible.samil.controller.background.SamilBackgroundColorDTO;
import com.belhyun.bible.samil.domain.background.SamilBackgroundRepositoryProvider;
import com.belhyun.bible.samil.domain.background.color.SamilBackgroundColor;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SamilBackgroundColorDataFetcher extends SamilGraphqlDataFetcher<List<SamilBackgroundColorDTO>> {

    @Autowired
    private SamilBackgroundRepositoryProvider repositoryProvider;

    @Override
    public List<SamilBackgroundColorDTO> get(DataFetchingEnvironment environment) {
        List<SamilBackgroundColorDTO> collect = repositoryProvider.getSamilBackgroundColorRepository().findAll()
                .stream()
                .map(SamilBackgroundColor::toDTO)
                .collect(Collectors.toList());

        return collect;
    }
}
