package com.belhyun.bible.samil.graphql.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class SamilGraphqlDataFetcherSet<T> {

    /**
     * Query, Mutation
     */
    private SamilGraphqlDataFetcherType type;

    private String key;

    private SamilGraphqlDataFetcher<T> dataFetcher;


}
