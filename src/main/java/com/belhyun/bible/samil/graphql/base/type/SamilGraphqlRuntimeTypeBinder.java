package com.belhyun.bible.samil.graphql.base.type;

import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcherSet;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcherType;
import graphql.schema.idl.RuntimeWiring;

import java.util.List;

public class SamilGraphqlRuntimeTypeBinder {

    public static RuntimeWiring.Builder bind(SamilGraphqlDataFetcherType type , RuntimeWiring.Builder builder, List<SamilGraphqlDataFetcherSet> dataFetcherList) {

        dataFetcherList.stream()
                .filter(f -> f.getType().equals(type))
                .forEach((fetcher) -> builder.type(type.getTypeName(),
                        typeWiring -> typeWiring
                                .dataFetcher(fetcher.getKey(), fetcher.getDataFetcher()

                                )
                        )
                );

        return builder;

    }
}
