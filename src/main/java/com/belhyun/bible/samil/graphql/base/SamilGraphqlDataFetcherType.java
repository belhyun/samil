package com.belhyun.bible.samil.graphql.base;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SamilGraphqlDataFetcherType {

    QUERY("Query"),

    MUTATION("Mutation");

    private String typeName;

    public String getTypeName() {
        return this.typeName;
    }

}
