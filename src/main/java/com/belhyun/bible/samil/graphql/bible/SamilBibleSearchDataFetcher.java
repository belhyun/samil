package com.belhyun.bible.samil.graphql.bible;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.domain.bible.repository.SamilBibleRepository;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcher;
import com.google.common.collect.Lists;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class SamilBibleSearchDataFetcher extends SamilGraphqlDataFetcher<List<SamilBible>> {

    @Autowired
    private SamilBibleRepository repository;

    @Override
    public List<SamilBible> get(DataFetchingEnvironment environment) {
        Optional<List<SamilBible>> samilBibles = repository.searchBibles(environment.getArgument("query"));
        return samilBibles.orElseGet(() -> Lists.newArrayList());
    }
}
