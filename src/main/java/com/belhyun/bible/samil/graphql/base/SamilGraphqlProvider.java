package com.belhyun.bible.samil.graphql.base;

import graphql.ExecutionInput;
import graphql.ExecutionResult;

import java.util.List;
import java.util.stream.Collectors;

public interface SamilGraphqlProvider {

    ExecutionResult execute(ExecutionInput input);

    ExecutionResult execute(String query);

    void loadSchema();

    default List<SamilGraphqlDataFetcherSet> getDataFetchers(List<SamilGraphqlDataFetcher> samilGraphqlDataFetchers) {
        return samilGraphqlDataFetchers.stream().map(
                fetcher -> {
                    SamilGraphQlDataFetcherRoute byClazz = SamilGraphQlDataFetcherRoute.getByClazz(fetcher.getClass());
                    return new SamilGraphqlDataFetcherSet(
                            byClazz.getFetcherType(),
                            byClazz.getRoute(),
                            fetcher);
                }).collect(Collectors.toList());

    }
}
