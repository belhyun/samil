package com.belhyun.bible.samil.graphql.bible;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.domain.bible.repository.SamilBibleRepository;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AllSamilBibleDataFetcher extends SamilGraphqlDataFetcher<List<SamilBible>> {

    @Autowired
    private SamilBibleRepository repository;

    @Override
    public List<SamilBible> get(DataFetchingEnvironment environment) {
        return repository.findAll();
    }
}
