package com.belhyun.bible.samil.graphql.bible;


import com.belhyun.bible.samil.graphql.base.SamilGraphqlDataFetcher;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlProvider;
import com.belhyun.bible.samil.graphql.base.SamilGraphqlSchemaLoader;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Component("SamilBibleGraphqlProvider")
public class SamilBibleGraphqlProvider implements SamilGraphqlProvider {


    @Autowired
    private List<SamilGraphqlDataFetcher> samilGraphqlDataFetchers;

    @Autowired
    private SamilGraphqlSchemaLoader loader;

    private GraphQL graphQL;


    @Override
    public ExecutionResult execute(ExecutionInput input) {
        return graphQL.execute(input);
    }

    @Override
    public ExecutionResult execute(String query) {
        return graphQL.execute(query);
    }

    @Override
    @PostConstruct
    public void loadSchema()  {

        Optional<GraphQL> graphQLOptional = loader.load("samilBible.graphql", getDataFetchers(samilGraphqlDataFetchers));

        if (graphQLOptional.isPresent()) {
            graphQL = graphQLOptional.get();
        }
    }
}
