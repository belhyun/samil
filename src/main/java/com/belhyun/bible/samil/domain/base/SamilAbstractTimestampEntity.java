package com.belhyun.bible.samil.domain.base;

import com.belhyun.bible.samil.batch.configuration.datasource.SamilEntityChangeableListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners(SamilEntityChangeableListener.class)
public abstract class SamilAbstractTimestampEntity {

    @Column(name = "created_at", columnDefinition = "DATETIME(3)")
    private LocalDateTime createdAt = LocalDateTime.now();

    @Column(name = "updated_at", columnDefinition = "DATETIME(3)")
    private LocalDateTime updatedAt;

    public LocalDateTime getCreated() {
        return createdAt;
    }

    public void setCreated(LocalDateTime created) {
        this.createdAt= created;
    }

    public LocalDateTime getUpdated() {
        return updatedAt;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updatedAt = updated;
    }
}
