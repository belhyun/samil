package com.belhyun.bible.samil.domain.bible.services;

import com.belhyun.bible.samil.controller.bible.search.SamilBibleGroupByNameDTO;

import java.util.List;
import java.util.Optional;

public interface SamilBibleService {

    /**
     * 성경의 장으로 그룹화한다.
     * @param version
     * @param query
     * @return
     */
    Optional<List<SamilBibleGroupByNameDTO>> searchBiblesGroupByName(String version, String query);

}
