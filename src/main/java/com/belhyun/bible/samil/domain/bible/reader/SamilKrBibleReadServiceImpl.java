package com.belhyun.bible.samil.domain.bible.reader;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.util.enumeration.SamilBibleLangEnum;
import com.belhyun.bible.samil.util.enumeration.SamilBibleWordReference;
import com.belhyun.bible.samil.util.file.SamilFileReader;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component("SamilKrBibleReadServiceImpl")
public class SamilKrBibleReadServiceImpl implements SamilBibleReadService {

    @Autowired
    private ResourcePatternResolver resourcePatternResolver;


    @Override
    public List<SamilBible> read() throws IOException {
        List<Resource> resourceList = Lists.newArrayList(resourcePatternResolver.getResources("classpath:bible/txt/kr/*.txt"));

        return resourceList.stream().map((r) -> {
            List<SamilBible> samilBibles = Lists.newArrayList();
            try {
                samilBibles = extract(SamilFileReader.readFromReader(new InputStreamReader(r.getInputStream())));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return samilBibles;

        }).flatMap(a -> a.stream()).collect(Collectors.toList());
    }

    private List<SamilBible> extract(String fullStr) {

        int WORD_REF = 1, CHAPTER = 3, VERSE = 4, WORD = 6;

        List<SamilBible> bibles= Lists.newArrayList();

        Iterator<String> iterator = Splitter.on("\r\n").split(fullStr).iterator();

        Pattern pattern = Pattern.compile("(\\D+)(\\s)?(\\d+):(\\d+)(\\s)?(.*+)");

        while (iterator.hasNext()) {
            Matcher matcher = pattern.matcher(iterator.next());
            while (matcher.find()) {
                bibles.add(SamilBible.builder()
                        .wordRef(SamilBibleWordReference.getBibleName(matcher.group(WORD_REF)).getMessage())
                        .lang(SamilBibleLangEnum.KR)
                        .chapter(matcher.group(CHAPTER))
                        .verse(matcher.group(VERSE))
                        .version("개역개정")
                        .name(SamilBibleWordReference.getBibleName(matcher.group(WORD_REF)).getDescription())
                        .word(matcher.group(WORD)).build());
            }
        }
        return bibles;
    }
}
