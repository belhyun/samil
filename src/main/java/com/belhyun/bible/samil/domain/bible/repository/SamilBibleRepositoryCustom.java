package com.belhyun.bible.samil.domain.bible.repository;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.util.enumeration.SamilBibleLangEnum;

import java.util.List;
import java.util.Optional;

public interface SamilBibleRepositoryCustom {

    Optional<SamilBible> findByChapterAndVerseAndVersionAndLang(String chapter, String verse, String version, SamilBibleLangEnum lang, String name);

    Optional<List<SamilBible>> searchBibles(String word);

}
