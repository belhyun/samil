package com.belhyun.bible.samil.domain.background.color;


import com.belhyun.bible.samil.controller.background.SamilBackgroundColorDTO;
import com.belhyun.bible.samil.domain.base.SamilAbstractTimestampEntity;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name="samil_background_color")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = { "id" })
@Data
public class SamilBackgroundColor extends SamilAbstractTimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /**
     * hex 값
     */
    @Column
    private String hex;

    /**
     * red
     */
    @Column
    private int red;

    /**
     * green
     */
    @Column
    private int green;

    /**
     * red
     */
    @Column
    private int blue;

    /**
     * 활성화 여부(1: 활성화, 0: 비활성화)
     */
    @Column
    private int active = 1;

    public SamilBackgroundColorDTO toDTO() {
        SamilBackgroundColorDTO dto = new SamilBackgroundColorDTO();
        dto.setHex(this.hex);
        dto.setRed(this.red);
        dto.setGreen(this.green);
        dto.setBlue(this.blue);
        return dto;
    }


}
