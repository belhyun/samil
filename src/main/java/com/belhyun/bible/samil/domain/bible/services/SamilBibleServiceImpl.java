package com.belhyun.bible.samil.domain.bible.services;

import com.belhyun.bible.samil.controller.bible.search.SamilBibleGroupByNameDTO;
import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.domain.bible.repository.SamilBibleRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.belhyun.bible.samil.util.cons.SamilComplement.NOT;

@Service
public class SamilBibleServiceImpl implements SamilBibleService {

    @Autowired
    private SamilBibleRepository samilBibleRepository;


    @Override
    public Optional<List<SamilBibleGroupByNameDTO>> searchBiblesGroupByName(String version, String query) {
        Optional<List<SamilBible>> samilBibles = samilBibleRepository.searchBibles(query);

        if (NOT(samilBibles.isPresent())) {
            return Optional.empty();
        }

        List<SamilBibleGroupByNameDTO> dtos = samilBibles.get().stream().reduce(Lists.newArrayList(), (acc, now) -> {

            SamilBibleGroupByNameDTO dto = acc.stream().filter((f) -> f.getName().equals(now.getName()))
                    .findFirst().orElseGet(() -> {
                        SamilBibleGroupByNameDTO initDto = new SamilBibleGroupByNameDTO();
                        initDto.setName(now.getName());
                        initDto.setVersion(now.getVersion());
                        initDto.setChapter(now.getChapter());
                        initDto.setGroupId(acc.size() + 1);
                        acc.add(initDto);
                        return initDto;
                    });
            dto.addSamilBibleIfNameMatch(now);
            return acc;
        },(a, b) -> a);

        if (dtos.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(
                dtos.stream().sorted(Comparator.comparing(SamilBibleGroupByNameDTO::getChapter)).collect(Collectors.toList())
        );
    }
}
