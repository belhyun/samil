package com.belhyun.bible.samil.domain.background.photo;


import com.belhyun.bible.samil.aws.s3.SamilS3UriExtractor;
import com.belhyun.bible.samil.controller.background.photo.SamilBackgroundPhotoDTO;
import com.belhyun.bible.samil.domain.base.SamilAbstractTimestampEntity;
import com.belhyun.bible.samil.util.rest.SamilRestDtoMaker;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name="samil_background_photo")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = { "id" })
@Data
public class SamilBackgroundPhoto extends SamilAbstractTimestampEntity implements SamilRestDtoMaker<SamilBackgroundPhotoDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 파일명
     */
    @Column
    private String fileName;

    @Column
    private String mimeType;

    /**
     * s3 URI
     */
    @Column
    private String s3Uri;

    /**
     * 활성화 여부(1: 활성화, 0: 비활성화)
     */
    @Column
    private int active = 1;

    @Override
    public SamilBackgroundPhotoDTO makeDto() {
        return SamilBackgroundPhotoDTO.builder()
                .s3Uri(this.s3Uri)
                .smallS3Uri(this.getSmallS3Uri())
                .mimeType(this.mimeType)
                .build();
    }

    private String getSmallS3Uri() {
        return SamilS3UriExtractor.GET_SMALL_S3URI_PATH(this.s3Uri);
    }
}
