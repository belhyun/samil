package com.belhyun.bible.samil.domain.background.photo;

import com.belhyun.bible.samil.controller.background.photo.SamilBackgroundPhotoDTO;
import com.belhyun.bible.samil.util.rest.SamilRestResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface SamilBackgroundPhotoService {


    SamilRestResponse<SamilBackgroundPhotoDTO> upload(@NotNull MultipartFile multipartFile);

    SamilRestResponse<List<SamilBackgroundPhotoDTO>> getSamilBackgroundPhotos();


}
