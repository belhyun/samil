package com.belhyun.bible.samil.domain.bible;

import com.belhyun.bible.samil.domain.base.SamilAbstractTimestampEntity;
import com.belhyun.bible.samil.util.enumeration.SamilBibleLangEnum;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name="samil_bible")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = { "id" })
@Data
public class SamilBible  extends SamilAbstractTimestampEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /**
     * 성경버전
     */
    @Column
    private String version;

    /**
     * 한글/영어(KR/EN)
     */
    @Column
    @Enumerated(EnumType.STRING)
    private SamilBibleLangEnum lang;

    /**
     * 성경약어(창)
     */
    @Column
    private String wordRef;

    /**
     * 성경 풀네임(창세기)
     */
    @Column
    private String name;

    /**
     * 장
     */
    @Column
    private String chapter;

    /**
     * 절
     */
    @Column
    private String verse;

    /**
     * 내용
     */
    @Column
    private String word;

    public static SamilBible getOne() {
        return SamilBible.builder().build();
    }

    @Override
    public String toString() {
        return lang.getMessage() + "=>" + version + "(" + name + " " + chapter + ":" + verse + ")";
    }
}
