package com.belhyun.bible.samil.domain.bible.repository;

import com.belhyun.bible.samil.domain.bible.QSamilBible;
import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.util.enumeration.SamilBibleLangEnum;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class SamilBibleRepositoryImpl extends QuerydslRepositorySupport implements SamilBibleRepositoryCustom {

    public SamilBibleRepositoryImpl() {
        super(SamilBible.class);
    }

    private final QSamilBible qSamilBible = QSamilBible.samilBible;

    @Override
    public Optional<SamilBible> findByChapterAndVerseAndVersionAndLang(String chapter, String verse, String version, SamilBibleLangEnum lang, String name) {
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(qSamilBible.chapter.eq(chapter));
        builder.and(qSamilBible.verse.eq(verse));
        builder.and(qSamilBible.name.eq(name));

        JPQLQuery<SamilBible> query = from(qSamilBible).where(builder).orderBy(qSamilBible.chapter.asc(), qSamilBible.verse.asc());

        return Optional.ofNullable(query.fetchOne());
    }

    @Override
    public Optional<List<SamilBible>> searchBibles(String query) {
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(
                qSamilBible.word.contains(query)
                .or(qSamilBible.name.contains(query))
        );
        JPQLQuery<SamilBible> jpqlQuery = from(qSamilBible).where(builder).orderBy(qSamilBible.chapter.asc(), qSamilBible.verse.asc());
        return Optional.ofNullable(jpqlQuery.fetch());
    }
}
