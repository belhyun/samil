package com.belhyun.bible.samil.domain.background.photo;

import com.belhyun.bible.samil.aws.s3.SamilS3UploadDTO;
import com.belhyun.bible.samil.aws.s3.SamilS3Uploader;
import com.belhyun.bible.samil.controller.background.photo.SamilBackgroundPhotoDTO;
import com.belhyun.bible.samil.domain.background.SamilBackgroundRepositoryProvider;
import com.belhyun.bible.samil.util.rest.SamilRestResponse;
import com.belhyun.bible.samil.util.rest.SamilRestResponseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

import static com.belhyun.bible.samil.controller.background.photo.SamilBackgroundPhotoResponseCode.SUCCESS;

@Service
public class SamilBackgroundPhotoServiceImpl implements SamilBackgroundPhotoService {

    @Autowired
    private SamilS3Uploader samilS3Uploader;

    @Autowired
    private SamilBackgroundRepositoryProvider repositoryProvider;

    private static final String UPLOAD_DIR = "BACKGROUND_PHOTO";

    @Override
    public SamilRestResponse<SamilBackgroundPhotoDTO> upload(@NotNull MultipartFile multipartFile) {

        SamilBackgroundPhoto savedOne = new SamilBackgroundPhoto();

        try {
            savedOne = repositoryProvider.getSamilBackgroundPhotoRepository().save(
                    SamilBackgroundPhoto.builder()
                    .active(1)
                    .mimeType(multipartFile.getContentType()).build());
            SamilS3UploadDTO samilS3UploadDTO = samilS3Uploader.upload(multipartFile, UPLOAD_DIR, savedOne.getId().intValue());

            savedOne.setFileName(samilS3UploadDTO.getUploadFileName());
            savedOne.setS3Uri(samilS3UploadDTO.getUploadImageUrl());
            repositoryProvider.getSamilBackgroundPhotoRepository().save(savedOne);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return SamilRestResponseFactory.getInstance().success(savedOne, SUCCESS);
    }

    @Override
    public SamilRestResponse<List<SamilBackgroundPhotoDTO>> getSamilBackgroundPhotos() {
        return SamilRestResponseFactory.getInstance().success(repositoryProvider.getSamilBackgroundPhotoRepository().findAll(), SUCCESS);
    }
}
