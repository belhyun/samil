package com.belhyun.bible.samil.domain.background;

import com.belhyun.bible.samil.domain.background.color.SamilBackgroundColorRepository;
import com.belhyun.bible.samil.domain.background.photo.SamilBackgroundPhotoRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public class SamilBackgroundRepositoryProvider {

    @Autowired
    private SamilBackgroundColorRepository samilBackgroundColorRepository;

    @Autowired
    private SamilBackgroundPhotoRepository samilBackgroundPhotoRepository;
}
