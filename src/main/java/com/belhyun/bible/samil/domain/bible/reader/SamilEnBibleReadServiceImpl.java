package com.belhyun.bible.samil.domain.bible.reader;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import com.belhyun.bible.samil.util.enumeration.SamilBibleLangEnum;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("SamilEnBibleReadServiceImpl")
public class SamilEnBibleReadServiceImpl implements SamilBibleReadService {

    @Autowired
    private ResourcePatternResolver resourcePatternResolver;

    @Override
    public List<SamilBible> read() throws IOException {

        int FIRST = 0;

        Resource resource = Lists.newArrayList(resourcePatternResolver.getResources("classpath:bible/excel/en/bibles_test.xlsx")).get(FIRST);

        List<SamilBible> result = Lists.newArrayList();

        XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(resource.getFile()));

        XSSFSheet sheet = workbook.getSheet("bibles");

        Map<Integer, String> versions = getVersions(sheet);

        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

            XSSFRow row = sheet.getRow(i);

            SamilBible samilBible = makeChapterAndVerse(row.getCell(FIRST).getStringCellValue());

            for (int j = 1; j < row.getPhysicalNumberOfCells(); j++) {
                SamilBible copyedSamilBible = SamilBible.getOne();

                BeanUtils.copyProperties(samilBible, copyedSamilBible);

                XSSFCell cell = row.getCell(j);
                copyedSamilBible.setVersion(versions.get(j));
                copyedSamilBible.setWord(cell.getStringCellValue());
                copyedSamilBible.setLang(SamilBibleLangEnum.EN);
                result.add(copyedSamilBible);
            }
        }
        return result;
    }

    private Map<Integer, String> getVersions(XSSFSheet sheet) {

        Map<Integer, String> versions = Maps.newHashMap();
        XSSFRow row = sheet.getRow(0);
        for (int i = 1; i < row.getPhysicalNumberOfCells(); i++) {
            versions.put(i, row.getCell(i).getStringCellValue());
        }
        return versions;
    }

    private SamilBible makeChapterAndVerse(String cell) {

        Pattern pattern = Pattern.compile("([0-9]?[\\D]+)(\\s)?(\\d+):(\\d+)");
        Matcher matcher = pattern.matcher(cell);

        SamilBible samilBible = null;
        while (matcher.find()) {

            samilBible = SamilBible.builder()
                    .name(matcher.group(1).replaceAll("\\s", ""))
                    .chapter(matcher.group(3))
                    .verse(matcher.group(4)).build();

        }
        return samilBible;
    }

}
