package com.belhyun.bible.samil.domain.background.photo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SamilBackgroundPhotoRepository  extends JpaRepository<SamilBackgroundPhoto, Long>, SamilBackgroundPhotoRepositoryCustom {
}
