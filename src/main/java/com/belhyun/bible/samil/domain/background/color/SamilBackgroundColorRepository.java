package com.belhyun.bible.samil.domain.background.color;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SamilBackgroundColorRepository extends JpaRepository<SamilBackgroundColor, Long>, SamilBackgroundColorRepositoryCustom {
}
