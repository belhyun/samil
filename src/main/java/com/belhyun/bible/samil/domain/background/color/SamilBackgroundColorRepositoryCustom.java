package com.belhyun.bible.samil.domain.background.color;

import java.util.Optional;

public interface SamilBackgroundColorRepositoryCustom {

    Optional<SamilBackgroundColor> getByHex(String hex);
}
