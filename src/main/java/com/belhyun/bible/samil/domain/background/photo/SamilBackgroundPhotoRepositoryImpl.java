package com.belhyun.bible.samil.domain.background.photo;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

@Repository
public class SamilBackgroundPhotoRepositoryImpl extends QuerydslRepositorySupport implements SamilBackgroundPhotoRepositoryCustom {

    private final com.belhyun.bible.samil.domain.background.photo.QSamilBackgroundPhoto qSamilBackgroundPhoto =
            com.belhyun.bible.samil.domain.background.photo.QSamilBackgroundPhoto.samilBackgroundPhoto;

    public SamilBackgroundPhotoRepositoryImpl() {
        super(SamilBackgroundPhoto.class);
    }

    @Override
    public int getMaxSeq() {

        SamilBackgroundPhoto samilBackgroundPhoto = from(qSamilBackgroundPhoto).orderBy(qSamilBackgroundPhoto.id.desc()).fetchFirst();

        if (samilBackgroundPhoto == null) {
            return 1;
        }

        return (int) (samilBackgroundPhoto.getId() + 1L);
    }
}
