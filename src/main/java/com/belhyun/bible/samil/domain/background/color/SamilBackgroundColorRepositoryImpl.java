package com.belhyun.bible.samil.domain.background.color;


import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class SamilBackgroundColorRepositoryImpl extends QuerydslRepositorySupport implements SamilBackgroundColorRepositoryCustom {

    private final com.belhyun.bible.samil.domain.background.color.QSamilBackgroundColor qSamilBackgroundColor =
            com.belhyun.bible.samil.domain.background.color.QSamilBackgroundColor.samilBackgroundColor;

    public SamilBackgroundColorRepositoryImpl() {
        super(SamilBackgroundColor.class);
    }

    @Override
    public Optional<SamilBackgroundColor> getByHex(String hex) {
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(
                qSamilBackgroundColor.hex.eq(hex)
        );
        JPQLQuery<SamilBackgroundColor> jpqlQuery = from(qSamilBackgroundColor).where(builder);
        return Optional.ofNullable(jpqlQuery.fetchOne());
    }
}
