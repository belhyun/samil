package com.belhyun.bible.samil.domain.bible.reader;

import com.belhyun.bible.samil.domain.bible.SamilBible;

import java.io.IOException;
import java.util.List;

public interface SamilBibleReadService {

    List<SamilBible> read() throws IOException;
}
