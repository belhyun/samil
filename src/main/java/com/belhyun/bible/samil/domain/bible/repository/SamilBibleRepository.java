package com.belhyun.bible.samil.domain.bible.repository;

import com.belhyun.bible.samil.domain.bible.SamilBible;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SamilBibleRepository extends JpaRepository<SamilBible, Long>, SamilBibleRepositoryCustom {



}
