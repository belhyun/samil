package com.belhyun.bible.samil.graphql.util;

import com.belhyun.bible.samil.graphql.SamilGQLQueryDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SamilGQLQueryBinderTest {


    @Test
    public void 바인더_테스트() {

        String test = "{\"operationName\":null,\"variables\":{\"query\":\"ground\"},\"query\":\"{\\n  searchSamilBibles(query: $query) {\\n    id\\n    chapter\\n    __typename\\n  }\\n}\\n\"}";


        SamilGQLQueryDTO parse = SamilGQLQueryBinder.parse(test);

        assertEquals(parse.getVariables().get("query"), "ground");


    }

}
