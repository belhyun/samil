package com.belhyun.bible.samil.batch.reader.bible;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SamilBibleToDBReaderIntegrationTest {

    @Autowired
    private SamilBibleToDBReader sut;

    @Test
    public void 파일_읽기_테스트() throws Exception {

        Assert.assertNotNull(sut.read());

    }

}