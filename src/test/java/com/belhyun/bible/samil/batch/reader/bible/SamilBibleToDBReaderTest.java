package com.belhyun.bible.samil.batch.reader.bible;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RunWith(MockitoJUnitRunner.class)
public class SamilBibleToDBReaderTest {

    @Test
    public void 정규식_테스트() {
        String text = "창1:4 빛이 하나님이 보시기에 좋았더라 하나님이 빛과 어둠을 나누사";

        Pattern pattern = Pattern.compile("(\\D+)(\\s)?(\\d+):(\\d+)(\\s)?(.*+)");

        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            Assert.assertEquals(matcher.group(1), "창");
            Assert.assertEquals(matcher.group(3), "1");
            Assert.assertEquals(matcher.group(4), "4");
            Assert.assertEquals(matcher.group(6), "빛이 하나님이 보시기에 좋았더라 하나님이 빛과 어둠을 나누사");
        }
    }

    @Test
    public void 정규식_테스트2() {

        String text = "Genesis 1:1";

        Pattern pattern = Pattern.compile("([0-9]?[\\D]+)(\\s)?(\\d+):(\\d+)");

        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            Assert.assertEquals(matcher.group(1).replaceAll("\\s", ""), "Genesis");
            Assert.assertEquals(matcher.group(3), "1");
            Assert.assertEquals(matcher.group(4), "1");
        }
    }

    @Test
    public void 정규식_테스트3() {

        String text = "1 Corinthians 4:17";

        Pattern pattern = Pattern.compile("([0-9]?[\\D]+)(\\s)?(\\d+):(\\d+)");

        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            Assert.assertEquals(matcher.group(1).replaceAll("\\s", ""), "1Corinthians");
            Assert.assertEquals(matcher.group(3), "4");
            Assert.assertEquals(matcher.group(4), "17");
        }
    }
}
