package com.belhyun.bible.samil.domain.bible.reader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SamilEnBibleReadServiceImplTest {

    @Resource(name = "SamilEnBibleReadServiceImpl")
    private SamilBibleReadService samilEnBibleReadServiceImpl;

    @Test
    public void 읽기_테스트() throws IOException {

        samilEnBibleReadServiceImpl.read();
    }

}
